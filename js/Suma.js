"use strict";
var Suma = /** @class */ (function () {
    function Suma(num1, num2, op) {
        this.num1 = num1;
        this.num2 = num2;
        this.op = op;
    }
    Suma.prototype.resultado = function () {
        return this.num1 + this.num2;
    };
    return Suma;
}());
module.exports = Suma;
