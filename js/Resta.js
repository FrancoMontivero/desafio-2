"use strict";
var Resta = /** @class */ (function () {
    function Resta(num1, num2, op) {
        this.num1 = num1;
        this.num2 = num2;
        this.op = op;
    }
    Resta.prototype.resultado = function () {
        return this.num1 - this.num2;
    };
    return Resta;
}());
module.exports = Resta;
