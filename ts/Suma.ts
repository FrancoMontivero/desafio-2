class Suma {

  private num1: number;
  private num2: number;
  private op: string;

  public constructor(num1: number, num2: number, op: string){
    this.num1 = num1;
    this.num2 = num2;
    this.op = op;
  }

  public resultado(): number {
    return this.num1 + this.num2
  }
}

export = Suma
