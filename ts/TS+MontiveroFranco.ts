interface Lote {
  num1: number;
  num2: number;
  operacion: string;
};

function operacion(num1: number, num2: number, op: string): Promise<string | Error> {
  return new Promise((resolve, reject): void => {
    op = op.trim().toLowerCase();
    if(op !== 'suma' && op !== 'resta'){
      reject(new Error("La operacion es invalida"));
    }
    import(`./${op[0].toUpperCase()}${op.slice(1)}`)
      .then(Operacion => {
        const instancia = new Operacion.default(num1, num2, op);
        resolve(instancia.resultado());
      })
  })
}

async function operaciones(ops: Array<Lote>): Promise<any>{
  for (let data of ops){
    console.log(`La ${data.operacion} de ${data.num1} y ${data.num2} es:`);
    console.log(await operacion(data.num1, data.num2, data.operacion));
  }
}

async function main(): Promise<any> {

  const lote1 : Lote = {
    num1 : 10,
    num2 : 20,
    operacion : "suma"
  }
  const lote2: Lote = {
    num1 : 20,
    num2 : 10,
    operacion : "suma"
  }
  const lote3: Lote = {
    num1 : 5,
    num2 : 4,
    operacion : "resta"
  }
  const lote4 : Lote = {
    num1 : 6,
    num2 : 5,
    operacion : "resta"
  }

  const lotesDePrueba: Array<Lote> = [lote1, lote2, lote3, lote4];
  await operaciones(lotesDePrueba);
}

export = main;
